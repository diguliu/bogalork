module DisplayHelper
  def state_to_board(state)
    x = state.split(//)
    board = [x[0..2], x[3..5], x[6..8]]
    "\n"+
    " #{board[0][0]} | #{board[0][1]} | #{board[0][2]} \n"+
    "---|---|--- \n"+
    " #{board[1][0]} | #{board[1][1]} | #{board[1][2]} \n"+
    "---|---|--- \n"+
    " #{board[2][0]} | #{board[2][1]} | #{board[2][2]} \n"+
    "\n"
  end

  def state_one_lined(state)
    state.board.join.gsub(' ', '-')
  end

  def multiples_states_row(states, start_value)
    per_line = states.size
    result = ""
    row = []
    per_line.times do |x|
      row << "#{x+start_value}) #{states[x].board[0][0]} | #{states[x].board[0][1]} | #{states[x].board[0][2]}"
    end
    result += row.join("  ")
    result += "\n"

    row = []
    per_line.times do |x|
      row << "  ---|---|---"
    end
    result += row.join(" ")
    result += "\n"

    row = []
    per_line.times do |x|
      row << "   #{states[x].board[1][0]} | #{states[x].board[1][1]} | #{states[x].board[1][2]}"
    end
    result += row.join("  ")
    result += "\n"

    row = []
    per_line.times do |x|
      row << "  ---|---|---"
    end
    result += row.join(" ")
    result += "\n"

    row = []
    per_line.times do |x|
      row << "   #{states[x].board[2][0]} | #{states[x].board[2][1]} | #{states[x].board[2][2]}"
    end
    result += row.join("  ")
    result += "\n"
    result
  end

  def multiples_states_per_line(options, per_line=3)
    result = "\n"
    states = Marshal.load(Marshal.dump(options))
    row = []
    start_value = 1
    while(!states.empty?)
      row << multiples_states_row(states.slice!(0..per_line-1), start_value)
      start_value += per_line
    end
    result += row.join("\n")
    result
  end
end
