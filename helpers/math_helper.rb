module MathHelper
  def sum(array)
    return 0 if array.empty?
    array.inject(0) { |sum, x| sum += x }
  end

  def mean(array)
    sum(array) / array.size.to_f
  end

  def mean_and_standard_deviation(array)
    m = mean(array)
    variance = array.inject(0) { |variance, x| variance += (x - m) ** 2 }
    return m, Math.sqrt(variance/(array.size-1))
  end
end
