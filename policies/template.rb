require 'core/policy'

class Policies::Template < Policy
  # Available attributes:
  #   + player
  #   + reward_function
  #   + knowledge (loaded knowledge)
  #   + base_knowledge (path to the knowledge to be loaded)
  #   + save_knowledge_to (path to the place where the knowledge must be saved)
  #   + action_save_knowledge (defines weather the knowledge should be saved or not)
  #
  #   Also all the dynamic attributes created in the config file
  #
  # Useful methods:
  #   + save_knowledge (save knowledge to the path specified on
  #   save_knowledge_to)
  #   + load_knowledge (load knowledge from the path specified on
  #   base_knowledge)
  #   + erase_knowledge (clean the knowledge attribute)

  # Function: set default values for attributes
  def load_defaults
  end

  # Function: choose an option from options based on the state
  # Return: State
  # Parameters:
  #   + state: State
  #   + options: Array<State>
  def decide(state, options)
  end

  # Function: Time to update de knowledge based on the step taken
  def analyze_step(player, state)
  end

  # Function: Time to update de knowledge based on the round played
  # Parameters:
  #   + round: Hash = { :player_sequence => <sequence-of-players-actions>,
  #                     :state_sequence => <sequence-of-states-passed>,
  #                     :finish => :win/:draw,
  #                     :winner => <player-or-group-of-playesr> }
  def analyze_round(round)
  end

  # Function: Time to update de knowledge based on the session played
  # Parameters:
  #   + session: Hash = {<round-number> => <round-as-specified-above>}
  def analyze_session(session)
  end

  # Function: Time to update de knowledge based on the training
  # Parameters:
  #   + training: Hash = {<session-number> => <session-as-specified-above>}
  def analyze_training(training)
  end
end
