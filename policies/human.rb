require 'core/policy'

class Policies::Human < Policy
  def decide(state, options)
    Interface.instance.display_policy_start_decide('Human', state)
    Interface.instance.display_human_policy_options(options)
    option = 0
    while(option < 1 || option > options.size)
      print ">> "
      option = gets.to_i
    end
    Interface.instance.display_human_policy_decision(options[option-1])
    options[option-1]
  end
end

