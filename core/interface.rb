require 'singleton'
require 'helpers/display_helper'
# require 'ap'

class Interface
  include Singleton
  include DisplayHelper
  
  attr_accessor :on

  def inform(information, method='puts')
    self.send(method, information)
  end

  def display(information, method='puts')
    return if !on
    self.send(method, information)
  end

  def display_game_moment(information)
    return if !on
    puts information[:state].to_s
    puts "=> #{information[:player]}"
  end

  def display_game_final(result)
    return if !on
    puts result
  end

  def display_final_state_reward(state, value)
    return if !on
    puts '=> Reward final state {'
    puts state
    puts "reward: #{value}"
    puts '}'
  end

  def display_policy_start_decide(policy, state)
    return if !on
    puts "==> #{policy} start decision from: #{state_one_lined(state)}"
  end

  def display_policy_option(state, reward)
    return if !on
    puts "#{state_one_lined(state)} (#{reward})"
  end

  def display_policy_decision(state, reward)
    return if !on
    puts "Decision: "
    puts state.to_s
    puts "# #{reward}"
  end

  def display_policy_random_decision(state, reward)
    return if !on
    puts "Random decision: "
    puts state.to_s
    puts "# #{reward}"
  end

  def display_td_start_update(state, knowledge, value)
    return if !on
    puts "==> TD update: #{state_one_lined(state)} {"
    puts "  > Current value: #{value}"
    ap knowledge
  end

  def display_td_end_update(knowledge, value)
    return if !on
    puts "  > Updated to: #{value}"
    ap knowledge
    puts '}'
  end

  def display_td_add_new_state(memory)
    return if !on
    puts "==> TD adding new state to memory {"
    memory.each do |item|
      Interface.instance.display_policy_option(item[:state], item[:reward])
    end
    puts "}"
  end

  def display_human_policy_options(options, per_line=3)
    puts multiples_states_per_line(options, per_line)
  end

  def display_human_policy_decision(state)
    return if !on
    puts "Decision: "
    puts state.to_s
  end

  def method_missing(method, *args, &block)
    if method.to_s =~ /^display_(.+)$/
      display(*args, &block)
    elsif method.to_s =~ /^inform_(.+)$/
      inform(*args, &block)
    else
      super
    end
  end
end
