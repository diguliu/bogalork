require 'yaml'
require 'core/state'

class RewardFunction
  include MetaHelper

  attr_accessor :knowledge, :base_knowledge, :save_knowledge_to, :action_save_knowledge

  def initialize(attrs)
    attrs.each do |attribute, value|
      self.class.module_eval { attr_accessor attribute.to_sym }
      self.send(attribute.to_s+'=', value)
    end
    erase_knowledge
    load_knowledge
    load_defaults
  end

  def load_defaults
    self.action_save_knowledge ||= false
  end

  def save_knowledge
    File.open(save_knowledge_to, 'w') {|f| f.write(knowledge.to_yaml) } if action_save_knowledge
  end

  def load_knowledge
    if base_knowledge && File.exists?(base_knowledge)
      self.knowledge = YAML.load_file(base_knowledge)
    end
  end

  def erase_knowledge
    self.knowledge = {}
  end

  def reward(player, state)
  end

  def analyze_step(player, state)
  end

  def analyze_round(round)
  end

  def analyze_session(session)
    save_knowledge
  end

  def analyze_training(training)
  end
end
