require 'yaml'
require 'helpers/meta_helper'

class Training
  include MetaHelper

  attr_accessor :game

  def initialize(config_file='training')
    attrs = YAML.load_file(File.join(Simulator::ROOT, 'configs', 'trainings', config_file+'.yml'))
    attrs.merge!(:name => 'training') if !attrs.has_key?(:name)
    attrs.merge!(:report_step => true) if !attrs.has_key?(:report_step)
    attrs.merge!(:report_round => true) if !attrs.has_key?(:report_round)
    attrs.merge!(:report_session => true) if !attrs.has_key?(:report_session)
    attrs.merge!(:report_training => true) if !attrs.has_key?(:report_training)
    attrs.merge!(:sessions => 1) if !attrs.has_key?(:sessions)
    attrs.merge!(:rounds => 100) if !attrs.has_key?(:rounds)
    attrs.merge!(:evaluate_train => false) if !attrs.has_key?(:evaluate_train)

    attrs.each do |attribute, value|
      self.class.module_eval { attr_accessor attribute.to_sym }
      self.send(attribute.to_s+'=', value)
    end
    self.class.send(:require, 'games/'+game)
    self.game = game_config ? constantize("Games::"+camelize(game)).new(game_config) : constantize("Games::"+camelize(game)).new
    start
  end

  def report_results(type)
    game.send("report_#{type}") if self.send("report_#{type}")
  end

  def start
    sessions.times do |session|
      Interface.instance.display_session_start("=======> Session #{session}")
      rounds.times do |round|
        Interface.instance.display_round_start("=======> Round #{round}")
        game.start(session, round)
        Interface.instance.display_game_moment(game.display_moment)
        while(!game.final?)
          game.register_action(game.current_player.play(game.current_state))
          report_results('step')
          Interface.instance.display_game_moment(game.display_moment)
        end
        game.close
        report_results('round')
        Interface.instance.inform_round_final("[#{name}] Round: #{round+1}/#{rounds}") if (round+1)%200 == 0
      end
      report_results('session')
        Interface.instance.inform_session_final("[#{name}] Session: #{session+1}/#{sessions}")
      #FIXME
      evaluate(session) if evaluate_train
    end
    report_results('training')
  end

  def evaluate(session)
    history = game.history[session]
    results = {:wins => 0, :draws => 0, :losts => 0}
    history.each do |id, round|
      if round[:finish] == :draw
        results[:draws] += 1
      elsif round[:winner] == game.players[0]
        results[:wins] += 1
      else
        results[:losts] += 1
      end
    end
    File.open(File.join(Simulator::ROOT, 'experiments', 'temp', 'results', "#{results_file}.yml"), 'w') {|f| f.write(results.to_yaml) }
  end
end
