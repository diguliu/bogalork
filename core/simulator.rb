require 'yaml'
require 'fileutils'
require 'helpers/meta_helper'
require 'helpers/math_helper'
require 'core/training'
require 'benchmark'

class Simulator
  include MetaHelper
  include MathHelper

  ROOT = File.expand_path('../..', __FILE__)

  def initialize(with_interface = false, interface='interface')
    self.class.send(:require, "core/#{interface}")
    constantize(camelize(interface)).instance.on = with_interface
  end

  def create_default_folders(sequence, game)
    dirs = [
      File.join(Simulator::ROOT, 'configs', 'trainings', sequence),
      File.join(Simulator::ROOT, 'configs', 'games'),
      File.join(Simulator::ROOT, 'configs', 'games', game),
      File.join(Simulator::ROOT, 'configs', 'games', game, sequence),
      File.join(Simulator::ROOT, 'experiments', sequence),
      File.join(Simulator::ROOT, 'experiments', 'temp'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'results'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'reward_functions'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'policies')
    ]
    dirs.each { |dir| Dir::mkdir(dir) if !File.exist?(dir)}
  end

  def erase_temp_folders
    dirs = [
      File.join(Simulator::ROOT, 'experiments', 'temp'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'results'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'reward_functions'),
      File.join(Simulator::ROOT, 'experiments', 'temp', 'policies')
    ]
    dirs.each { |dir| FileUtils.rm_rf(File.join(dir, '*'))}
  end

  def erase_config_folders(sequence, game)
    dirs = [
      File.join(Simulator::ROOT, 'configs', 'trainings', sequence),
#      File.join(Simulator::ROOT, 'configs', 'games'),
#      File.join(Simulator::ROOT, 'configs', 'games', game),
      File.join(Simulator::ROOT, 'configs', 'games', game, sequence),
    ]
    dirs.each { |dir| FileUtils.rm_rf(dir)}
  end

  def save_information(sequence)
    path = File.join(Simulator::ROOT, 'experiments', sequence)
    next_dir = Dir.glob(File.join(path, '*')).map {|dir| File.basename(dir)}.max.to_i + 1
    path = File.join(path, next_dir.to_s)
    Dir::mkdir(path)
    FileUtils.mv(Dir.glob(File.join(Simulator::ROOT, 'experiments', 'temp', '*')), path)
    FileUtils.cp(File.join(Simulator::ROOT, 'configs', 'sequences', sequence+'.yml'), File.join(path, 'training_config.yml'))
  end

  def generate_chart(result)
    file = File.open(File.join(Simulator::ROOT, 'experiments', 'template.html'), 'r').read
    file.gsub!('-TITLE-', result[:training_name])
    file.gsub!('-SUBTITLE-', '')
    file.gsub!('-XAXIS-', 'Rounds')
    file.gsub!('-YAXIS-', 'Result (%)')
    series ="[
      { name: 'Wins',
        data: #{result[:results].sort.map { |values| [values[0], values[1][:mean_wins]]}.inspect} },
      { name: 'Losts',
        data: #{result[:results].sort.map { |values| [values[0], values[1][:mean_losts]]}.inspect} },
      { name: 'Draws',
        data: #{result[:results].sort.map { |values| [values[0], values[1][:mean_draws]]}.inspect} },
    ]"
    file.gsub!('-SERIES-', series)
    File.open(File.join(Simulator::ROOT, 'experiments', 'temp', 'results', 'final.html'), 'w') {|f| f.write(file) }
  end

  def calculate_results(round_checkpoints, number_of_trainings, sequence)
    result = {}
    result[:results] = {}
    round_checkpoints.each do |checkpoint|
      wins = []
      draws = []
      losts = []
      number_of_trainings.times do |i|
        evaluation = YAML.load_file(File.join(Simulator::ROOT, 'experiments', 'temp', 'results', i.to_s, "evaluation#{checkpoint}.yml"))
        wins << evaluation[:wins]
        draws << evaluation[:draws]
        losts << evaluation[:losts]
      end
      result[:results][checkpoint] = {}
      if number_of_trainings > 1
        result[:results][checkpoint][:mean_wins], result[:results][checkpoint][:mean_deviation_wins] = mean_and_standard_deviation(wins)
        result[:results][checkpoint][:mean_draws], result[:results][checkpoint][:mean_deviation_draws] = mean_and_standard_deviation(draws)
        result[:results][checkpoint][:mean_losts], result[:results][checkpoint][:mean_deviation_losts] = mean_and_standard_deviation(losts)
      else
        result[:results][checkpoint][:mean_wins], result[:results][checkpoint][:mean_deviation_wins] = wins[0], 0
        result[:results][checkpoint][:mean_draws], result[:results][checkpoint][:mean_deviation_draws] = draws[0], 0
        result[:results][checkpoint][:mean_losts], result[:results][checkpoint][:mean_deviation_losts] = losts[0], 0
      end
    end
    result[:number_of_executions] = number_of_trainings
    result[:training_name] = sequence

    File.open(File.join(Simulator::ROOT, 'experiments', 'temp', 'results', 'final.yml'), 'w') {|f| f.write(result.to_yaml) }
    generate_chart(result)
    Interface.instance.display_evaluation_results(result, 'ap')
    return
  end

  def build_training_configs(configs, i, sequence, checkpoint, rounds)
    configs[:rounds] = rounds
    configs[:evaluate_train] = false
    configs[:name] = "(#{i}) Train #{checkpoint}"
    File.open(File.join(Simulator::ROOT, 'configs', 'trainings',sequence, "training#{checkpoint}.yml"), 'w') {|f| f.write(configs.to_yaml) }
  end

  def build_evaluation_configs(configs, i, sequence, checkpoint)
    configs[:evaluate_train] = true
    configs[:report_step] = false
    configs[:report_round] = false
    configs[:report_session] = false
    configs[:report_training] = false
    configs[:results_file] = File.join(i.to_s, "evaluation#{checkpoint}")
    configs[:name] = "(#{i}) Evaluation #{checkpoint}"
    File.open(File.join(Simulator::ROOT, 'configs', 'trainings', sequence, "evaluation#{checkpoint}.yml"), 'w') {|f| f.write(configs.to_yaml) }
  end

  def build_game_training_config(configs, i, sequence, game)
    path = File.join('experiments', 'temp', 'reward_functions', "train#{i}.yml")
    p=1
    configs[:reward_function][:base_knowledge] = path
#     configs[:reward_function][:base_knowledge] ||= path
    configs[:reward_function][:save_knowledge_to] = path
#     configs[:reward_function][:save_knowledge_to] ||= path
    configs[:players].each do |player|
      path =  File.join('experiments', 'temp', 'policies', "player#{p}_train#{i}.yml")
      player[:policy_options][:save_knowledge_to] = path
#       player[:policy_options][:save_knowledge_to] ||= path
      player[:policy_options][:base_knowledge] = path
#       player[:policy_options][:base_knowledge] ||= path
      p+=1
    end
    File.open(File.join(Simulator::ROOT, 'configs', 'games', game, sequence, "train#{i}.yml"), 'w') {|f| f.write(configs.to_yaml) }
  end

  def build_game_evaluation_config(configs, i, sequence, game)
      p=1
      configs[:reward_function][:base_knowledge] = File.join('experiments', 'temp', 'reward_functions', "train#{i}.yml")
#       configs[:reward_function][:base_knowledge] ||= File.join('experiments', 'temp', 'reward_functions', "train#{i}.yml")
      configs[:players].each do |player|
        path =  File.join('experiments', 'temp', 'policies', "player#{p}_train#{i}.yml")
        player[:policy_options][:base_knowledge] = path
#         player[:policy_options][:base_knowledge] ||= path
        p+=1
      end
      File.open(File.join(Simulator::ROOT, 'configs', 'games', game, sequence, "evaluate#{i}.yml"), 'w') {|f| f.write(configs.to_yaml) }
      return configs
  end

  def run_sequence(sequence)
    Interface.instance.inform_start_sequence("\n\n=== Starting #{sequence} ===\n\n")
    configs = YAML.load_file(File.join(Simulator::ROOT, 'configs', 'sequences', sequence+'.yml'))

    configs[:training_config] ||= {}
    configs[:evaluation_config] ||= {}
    configs[:training_config][:game] = configs[:game]
    configs[:evaluation_config][:game] = configs[:game]

    run_training = configs.has_key?(:run_training) ? configs[:run_training] : true
    run_evaluation = configs.has_key?(:run_evaluation) ? configs[:run_evaluation] : true

    create_default_folders(sequence, configs[:game])
    erase_temp_folders

    configs[:number_of_trainings].times do |i|
      dir = File.join(Simulator::ROOT, 'experiments', 'temp', 'results', i.to_s)
      Dir::mkdir(dir) if !File.exist?(dir)

      build_game_training_config(configs[:game_training_config], i, sequence, configs[:game]) if run_training
      build_game_evaluation_config(configs[:game_evaluation_config], i, sequence, configs[:game]) if run_evaluation

      configs[:training_config][:game_config] = File.join(sequence, "train#{i}")
      configs[:evaluation_config][:game_config] = File.join(sequence, "evaluate#{i}")

      last_checkpoint = 0
      configs[:round_checkpoints].each do |checkpoint|
        rounds = checkpoint-last_checkpoint
        
        if run_training
          build_training_configs(configs[:training_config], i, sequence, checkpoint, rounds)
          Training.new(File.join(sequence, "training#{checkpoint}")) if run_training
        end
        if run_evaluation
          build_evaluation_configs(configs[:evaluation_config], i, sequence, checkpoint) if run_evaluation
          Training.new(File.join(sequence, "evaluation#{checkpoint}")) if run_evaluation
        end
        last_checkpoint = checkpoint
      end
    end

    calculate_results(configs[:round_checkpoints], configs[:number_of_trainings], sequence)
    save_information(sequence)
    erase_config_folders(sequence, configs[:game])
    erase_temp_folders
  end

  def run_training_sequences(sequences)
    sequences = [sequences] if !sequences.kind_of?(Array)
      sequences.each do |sequence|
        time_unit = 'seconds'
        time = (Benchmark.realtime { run_sequence(sequence) })
        if time > 60
          time /= 60.0
          time_unit = 'minutes'
          if time > 60
            time /= 60.0
            time_unit = 'hours'
          end
        end
        puts "Elapsed time: #{time} #{time_unit}"
      end
    return
  end
end
