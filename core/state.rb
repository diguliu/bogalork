require 'digest/sha2'

class State
  attr_accessor :player, :board, :hash

  def initialize(player, board=nil)
    self.player = player
    self.board = board ? board : initial_board 
  end

  # State model should fill the initial board
  def initial_board
  end

  def clone_board
    Marshal::load(Marshal.dump(board))
  end

  def id
    self.hash ||= (Digest::SHA2.new << (YAML::dump(board)+player.id.to_s)).to_s
  end

  # Display game state in a graphical way
  def to_s
  end

  def next_possible_states
  end

  def check
  end
end
