require 'yaml'
require 'core/player'

class Game
  include MetaHelper

  attr_accessor :configs, 
                :current_player, 
                :previous_player, 
                :current_state, 
                :players, 
                :result, 
                :history, 
                :reward_function,
                :session,
                :round

  def initialize(config_file='default')
    self.configs = YAML.load_file(File.join(Simulator::ROOT, 'configs', underscore(self.class.name), "#{config_file}.yml"))
    self.players = []
    self.history = {}
    initialize_reward_function
    i = 0
    configs[:players].each do |player|
      self.players << Player.new(i, player, reward_function)
      i += 1
    end
    load_defaults
  end

  def load_defaults
  end

  def initialize_reward_function
    name = configs[:reward_function] && configs[:reward_function][:name] ? configs[:reward_function][:name] : 'default'
    self.class.send(:require, underscore(self.class.name)+'/reward_functions/'+name)
    self.reward_function = constantize(camelize(self.class.name)+"::RewardFunctions::"+camelize(name)).new(configs[:reward_function] || {})
  end

  def start(session, round)
    self.session = session
    self.round = round
    self.history[session] = {} if !history.has_key?(session)
    self.history[session][round] = {:player_sequence => [], :state_sequence => []}
    self.result = nil
    self.current_player = first_player
    self.current_state = first_state
    save_step
  end

  def close
    self.history[session][round][:player_sequence].pop
    if winner
      self.history[session][round][:finish] = :win
      self.history[session][round][:winner] = winner
    else
      self.history[session][round][:finish] = :draw
    end
    Interface.instance.display_game_final(display_final)
  end

  def register_action(state)
    self.previous_player = current_player
    self.current_player = next_player
    self.current_state = state
    save_step
  end

  def save_step
    self.history[session][round][:player_sequence] << current_player
    self.history[session][round][:state_sequence] << current_state
  end

  def display_moment
    {:state => current_state, :player => current_player.name}
  end

  def display_final
    if winner
      "= The winner is #{winner.name} ="
    else
      '= Draw game ='
    end
  end

  def first_player
    players.first
  end

  def first_state
    self.class.send(:require, underscore(self.class.name)+'/states/'+configs[:state_model])
    constantize(self.class.name + '::States::' + camelize(configs[:state_model])).new(current_player)
  end

  def next_player
    # simulates a round robin order
    players[(players.index(current_player)+1) % players.size]
  end

  def final?
    self.result = current_state.check
    result[:final]
  end

  def winner
    if result.has_key?(:winner)
      players.select { |player| player.symbol == result[:winner] }.first
    end
  end

  # Might think if there should be an option for different types of reports
  # like specific reports
  # collective_report(report[:results], type) if report[:delivery] == :colletive
  # specific_report(report[:results], type, report[:target]) if report[:delivery] == :specific
  #
  # def collective_report(results, type)
  #   game.players.each do |player|
  #     player.send("analyze_#{type}_results", results)
  #   end
  # end
  #
  # def specific_report(results, type, player)
  #   player.send("analyze_#{type}_results", results)
  # end

  def report_step
    reward_function.analyze_step(previous_player, current_state)
    players.each do |player|
      player.analyze_step(previous_player, current_state)
    end
  end

  def report_round
    reward_function.analyze_round(history[session][round])
    players.each do |player|
      player.analyze_round(history[session][round])
    end
  end

  def report_session
    reward_function.analyze_session(history[session])
    players.each do |player|
      player.analyze_session(history[session])
    end
  end

  def report_training
    reward_function.analyze_training(history)
    players.each do |player|
      player.analyze_training(history)
    end
  end
end
