require 'policies'

class Player
  include MetaHelper
  attr_accessor :id, :name, :policy

  def initialize(id, attrs, reward_function)
    self.id = id
    attrs.each do |attribute, value|
      self.class.module_eval { attr_accessor attribute.to_sym }
      self.send(attribute.to_s+'=', value)
    end
    self.name ||= "Player#{id}"
    initialize_policy(reward_function)
  end

  def initialize_policy(reward_function)
    if policy_options && policy_options[:name]
      self.class.send(:require, 'policies/'+policy_options[:name])
      self.policy = constantize('Policies::'+camelize(policy_options[:name])).new(policy_options, reward_function)
    else
      self.class.send(:require, 'policies/default')
      self.policy = Policies::Default.new({}, reward_function)
    end
    policy.player = self
  end

  def analyze_step(player, state)
    policy.analyze_step(player, state)
  end

  def analyze_round(round)
    policy.analyze_round(round)
  end

  def analyze_session(session)
    policy.analyze_session(session)
  end

  def analyze_training(training)
    policy.analyze_training(training)
  end

  def play(state)
    options = state.next_possible_states(self)
    decision = policy.decide(state, options)
    return decision
  end
end
