require 'games'
require 'core/game'

class Games::Template < Game
  # Available attribute:
  #   + configs (all attributes defined in the config file)
  #   + current_player
  #   + previous_player
  #   + current_state
  #   + players (list of players)
  #   + result (current result of the game)
  #   + history  (history of execution)
  #   + reward_function
  #   + session
  #   + round

  # Function: defines who's the first player based on the session and the round.
  # Default: first player of the players' list.
  # Return: Player
  def first_player
  end

  # Function: defines who's the next player to play.
  # Default: round robin over the list of players.
  # Return: Player
  def next_player
  end
end
