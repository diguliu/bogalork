require 'games'
require 'core/game'

class Games::TicTacToe < Game
  def first_player
    players[round%2]
  end
end
