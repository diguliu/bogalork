require 'core/state'
require 'games/tic_tac_toe/states'

class Games::TicTacToe::States::ClassicState < State

  def initial_board
    [ [' ', ' ', ' '],
      [' ', ' ', ' '],
      [' ', ' ', ' '] ]
  end

  def id
    hash ||= player.id.to_s+board.join
  end

  def next_possible_states(next_player)
    states = []
    [0,1,2].each do |line|
      [0,1,2].each do |row|
        if board[line][row] == ' '
          new_board = clone_board
          new_board[line][row] = next_player.symbol
          states << Games::TicTacToe::States::ClassicState.new(next_player, new_board)
        end
      end
    end
    return states
  end

  def check
    empty_space = false
    winner = nil
    (board + board.transpose + 
     [ [board[0][0], board[1][1], board[2][2]],
       [board[0][2], board[1][1], board[2][0]] ]).each do |line|
      break if winner 
      result = check_line(line)
      if result == 's'
        empty_space = true
      elsif result != 'n'
        winner = result
      end
    end
    return {:final => true, :winner => winner} if winner
    return {:final => false} if empty_space
    return {:final => true}
  end

  def check_line(line)
    # s = not win and there is a space
    # n = not win and there is not a space
    m = nil
    line.each do |p|
      return 's' if p == ' '
      m ? (m='n' if m != p) : m=p
    end
    return m
  end

  def to_s
    "\n"+
    " #{board[0][0]} | #{board[0][1]} | #{board[0][2]} \n"+
    "---|---|--- \n"+
    " #{board[1][0]} | #{board[1][1]} | #{board[1][2]} \n"+
    "---|---|--- \n"+
    " #{board[2][0]} | #{board[2][1]} | #{board[2][2]} \n"+
    "\n"
  end
end
