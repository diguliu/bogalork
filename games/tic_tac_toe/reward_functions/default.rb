require 'games/tic_tac_toe/reward_functions'
require 'core/reward_function'

class Games::TicTacToe::RewardFunctions::Default < RewardFunction
  def reward(player, state)
    knowledge.has_key?(state.id) ? knowledge[state.id] * multiplier(player) : 0
  end

  def reward_value
    1
  end

  def multiplier(player)
    player.symbol == 'X' ? 1 : -1
  end

  def analyze_round(round)
    knowledge.has_key?(:rounds) ? knowledge[:rounds] += 1 : knowledge[:rounds] = 1
    return if !round[:winner]
    round[:state_sequence].each do |state|
      knowledge[state.id] = 0 if !knowledge.has_key?(state.id)
      knowledge[state.id] += reward_value * multiplier(round[:winner])
    end
  end
end
